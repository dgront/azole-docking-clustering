#load by_HEM_iron.pdb
#load by_HEM_00040.pdb

bg_color white
util.cbss("all","salmon", "skyblue","gray",_self=cmd)
select hem, resn hem
color smudge, hem
set_bond stick_radius, 0.3, hem
set sphere_scale, 0.3

show spheres, hem
show sticks, hem

select iron, element fe
alter iron, vdw=3.0
rebuild iron
show spheres, iron
color tv_red, iron

ray 2500, 2000
#png all.png

