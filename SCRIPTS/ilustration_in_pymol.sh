for i in templatee_*.pdb
do
  j=`basename $i .pdb`
  echo "load by_HEM_iron.pdb" >m.pml
  echo "load " $i >> m.pml
  cat ../../superimpose/ligand_protein_in_pymol.pml >> m.pml
  echo "png " $j.png >>m.pml
  echo "quit" >> m.pml
  pymol -cq m.pml
done
