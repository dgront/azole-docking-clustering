import sys, glob

amino_acid_code3_to_code1 = {'ALA': 'A', 'ARG': 'R', 'ASN': 'N', 'ASP': 'D',
                    'CYS': 'C', 'GLU': 'E', 'GLN': 'Q', 'GLY': 'G',
                    'HIS': 'H', 'ILE': 'I', 'LEU': 'L', 'LYS': 'K',
                    'MET': 'M', 'PHE': 'F', 'PRO': 'P', 'SER': 'S',
                    'THR': 'T', 'TRP': 'W', 'TYR': 'Y', 'VAL': 'V'}
# hbonds example:
# A  158   ALA  H   ->  F2  VT_    1  X : 2.724 3.678 157.57  110.18
H_columns = [(2, 1), (6, 7)]                # res_name, res_no

# stacking example:
# X    1  VT_     A  139  PHE    6.41   0.80 159.96   6.46
S_columns = [(2, 1), (5, 4)]                # res_name, res_no

# vdW example:
# A  103  ARG  NH2    X    1  VT_  F7     2.99
V_columns = [(2, 1), (6, 5)]                # res_name, res_no

ligands = ["Ebu", "VT_"]
proteins = ["00040", "1742", "271374", "345805", "367271", "38441", "523016", "627561", "629036", "809380", "EAL23379", "KIR77383"]

def parse_file(fname, columns):
    inter = set()
    for line in open(fname):
        tokens = line.strip().split()
        res_no_1 = tokens[columns[0][1]]
        res_name_1 = tokens[columns[0][0]]
        res_no_2 = tokens[columns[1][1]]
        res_name_2 = tokens[columns[1][0]]
        if res_name_1 in ligands:
            res_name_2 = amino_acid_code3_to_code1.get(res_name_2,res_name_2)
            res = "%s%s" % (res_name_2, res_no_2)
        else:
            res_name_1 = amino_acid_code3_to_code1.get(res_name_1,res_name_1)
            res = "%s%s" % (res_name_1, res_no_1)
        inter.add(res)
    return inter


files = set()
for f in glob.glob("*.pdb"):
    files.add(f.split('.')[0]+"."+f.split('.')[1])

for f in files:
    out = open(f+".interactions", "w")
    print("hbonds : ", end="", file=out)
    for i in parse_file(f+".hbonds", H_columns):
        print(i, end=",", file=out)
    print(file=out)
    print("stacking : ", end="", file=out)
    for i in parse_file(f+".stacking", S_columns):
        print(i, end=",", file=out)
    print( file=out)
    print("vdw : ", end="", file=out)
    for i in parse_file(f+".vdw", V_columns):
        print(i, end=",", file=out)
    print( file=out)
