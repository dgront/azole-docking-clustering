PML_SCRIPT="ligand_protein_in_pymol.pml"
#PML_SCRIPT="ligand_protein_zoom_in_pymol.pml"
MASK="templatee_00040_HEM_Clotrimazole.pdb_8590.pdb.pdb"
#MASK="*HEM_*.pdb"
#MASK="by_HEM_*.pdb"
for i in $MASK
do
  rm m.pml
  j=`basename $i .pdb`
#  echo "load by_HEM_iron.pdb" >m.pml
  echo "load " $i >> m.pml
  cat $PML_SCRIPT >> m.pml
  echo "png " $j.png >>m.pml
#  echo "quit" >> m.pml
  pymol m.pml
done

