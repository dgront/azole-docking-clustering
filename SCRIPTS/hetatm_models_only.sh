#!/bin/bash

#PBS -V
#PBS -l walltime=720:00:00
#PBS -o $PBS_O_WORKDIR/stdout
#PBS -e $PBS_O_WORKDIR/stderr
#cd $PBS_O_WORKDIR


O_DIR="./o"
#for i in `find $O_DIR -name *.pdb`
echo > fnames
for i in `ls $O_DIR/*.pdb`
do
  echo $i >> fnames
  echo "MODEL  1"
  grep "HETATM" $i
  echo "ENDMDL"
done > het.pdb
