import sys, argparse, glob

sys.path.append("/home/users/dgront/runs/Akapo-results/azole-docking-clustering")
sys.path.append("/home/dgront/src.git/bioshell/bin/")

from pybioshell.core.data.io import Pdb
from pybioshell.core.data.structural import PdbAtom

# On funk, use this python to run it:
# /home/users/jkrys/src.git/Python-3.7.2/python
# remember to set path:
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/users/jkrys/src.git/Python-3.7.2/

# On bioshell.pl:


def pdb_name_to_key_size(pdb_file_name):
    fname = pdb_file_name.split("/")[-1]
    tokens = fname.split("-")  # e.g. 629036-Clotrimazole-5.0-first-c10-11.pdb
    return tokens[0] + tokens[1] + tokens[2] + tokens[4], int(tokens[5].split(".")[0])


def measure_distances(fname, hem_chain='B', ligand_chain='X'):
    """ Reads a PDB file and calculates minimal distances between a ligand molecule and FE atom

    Calculates the closest distance between FE atom and any atom of a ligand as well as the closest distance between
    FE atom and any nitrogen atom.

    :param fname:  name of the input PDB file
    :param hem_chain: ID of a chain where HEM residue is (B in this experiment)
    :param ligand_chain:  ID of a chain where a ligand residue is (B in this experiment)
    :return: a tuple of four: min distance, closest atom name, min nitrogen distance, closest nitrogen atom name
    """
    p = Pdb(fname, "", False, False)
    if p.count_models() > 1:
        print("More than one model found in: %s\nOnly the first one will be processed", fname, file=sys.stderr)

    s = p.create_structure(0)
    ligand = s.get_chain(ligand_chain)[0]     # Assume the PDB has chain 'X' with just one residue - the ligand
    hem = s.get_chain(hem_chain)[0]           # Assume the PDB has chain 'B' with just one residue - the HEM molecule
    try :
        fe = hem.find_atom_safe("FE1 ")
    except:
        print("Can't find FE atom in ", hem.residue_type(), file=sys.stderr)
        return ""
    min_dist = 1000.0
    min_n    = 1000.0
    min_d_name = " - "
    min_n_name = " - "
    for aj in range(ligand.count_atoms()):
        d = fe.distance_to(ligand[aj])
        if min_dist > d :
            min_dist = d
            min_d_name = ligand[aj].atom_name()
        if ligand[aj].atom_name()[1] == "N" and min_n > d:
            min_n = d
            min_n_name = ligand[aj].atom_name()
    return min_dist, min_d_name, min_n, min_n_name


def extract_energies(fname):
    """ Read a Rosetta's output PDB file and extracts score values.

    Returns a tuple of three lists: score labels, pose scores and ligand scores. Each list should have the same number
    of elements.
    :param fname: name of the input PDB file
    :return: ``tuple(list, list, list)`` that holds score labels, pose scores and ligand scores
    """
    for line in open(fname):
        if line.startswith("label"):
            labels = line.strip().split()[1:]
        elif line.startswith("pose"):
            pose = line.strip().split()[1:]
        elif line.find("_ligand") > 0:
            ligand = line.strip().split()[1:]

    return labels, pose, ligand


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Reads PDB files produced by Rosetta and prepares score/distance statistics')
    parser.add_argument('-f', '--pdb', help="input PDB file", nargs='+', required=False)
    parser.add_argument('-p', '--path', help="path where the PDB files are located", type=str,
                        nargs=1, required=False)
    args = parser.parse_args()

    input_files = []
    if args.pdb: input_files.extend(args.pdb)
    if args.path:
        for fname in glob.glob(args.path[0], recursive=True):
            if fname.endswith(".pdb"):
                print(fname)
                input_files.append(fname)

    full_score_file = open("hires_pose_scores.out", "w")
    lgnd_score_file = open("hires_ligand_scores.out", "w")
    print_header = True
    for fname in input_files:
        fname_only = fname.split('/')[-1]
        min_dist, min_d_name, min_n, min_n_name = measure_distances(fname, 'B', 'X')
        label_names, pose_scores, ligand_scores = extract_energies(fname)
        if print_header:
            for l in ["SCORE:", *label_names, "min_distance", "atom_name", "min_N_distance", "N_name", "tag"]:
                full_score_file.write(l+' ')
                lgnd_score_file.write(l+' ')
            lgnd_score_file.write('\n')
            full_score_file.write('\n')
            print_header = False # --- write header only once
        full_score_file.write("SCORE: ")
        lgnd_score_file.write("SCORE: ")
        for v in pose_scores: full_score_file.write(str(v) + ' ')
        for v in [min_dist, min_d_name, min_n, min_n_name]: full_score_file.write(str(v) + ' ')
        full_score_file.write(fname_only+'\n')
        for v in ligand_scores: lgnd_score_file.write(str(v) + ' ')
        for v in [min_dist, min_d_name, min_n, min_n_name]: lgnd_score_file.write(str(v) + ' ')
        lgnd_score_file.write(fname_only+'\n')
    lgnd_score_file.close()
    full_score_file.close()



