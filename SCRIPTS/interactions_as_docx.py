import glob

proteins = ["00040", "1742", "271374", "345805", "367271", "38441", "523016", "627561", "629036", "809380", "EAL23379", "KIR77383"]
drugs = ["Clotrimazole", "Eburicol", "Fluconazole", "Itraconazole", "Ketoconazole", "Lanosterol", "Obtusifoliol", "VT-1129", "Voriconazole"]

from docx import Document
from docx.shared import RGBColor

document = Document()
r = len(proteins) # Number of rows you want
c = len(drugs) # Number of collumns you want
table = document.add_table(rows=r+1, cols=c+2)
#table.style = 'LightShading-Accent1' # set your style, look at the help documentation for more help

for i_row in range(r):
    cell = table.cell(i_row+1, 0)
    cell.text = proteins[i_row]
for i_col in range(c):
    cell = table.cell(0,i_col+2)
    cell.text = drugs[i_col]

observed = {}
in_pocket = {}
i_row = 1
for p in proteins:
    res = open(p+".pocket").read().replace(',', ' ')
    in_pocket[p] = res.split()
    par = table.cell(i_row, 1).add_paragraph(res, style='Normal')
    i_row += 1

i_col = 2
for l in drugs:
    for f in glob.glob(l+"/*.interactions"):
        protein = f.replace('/','_').split('_')[1]

        if l+protein in observed: continue
        else: observed[l+protein] = True

        try:
            i_row = proteins.index(protein) + 1
        except:
            continue
        lines = open(f).read()
        intrct = {}
        for line in lines.split('\n'):
            tokens = line.split(':')
            if len(tokens) > 1:
                name = tokens[0].strip()
                intrct[name] = tokens[1].split(',')
                print(protein, l, tokens[0].strip(),intrct[tokens[0].strip()])
        h_txt = ""
        v_txt = ""
        s_txt = ""
        print(intrct)
        if "hbonds" in intrct:
            for t in intrct["hbonds"]:
                if len(t) > 0 : h_txt += t + " "
        if "stacking" in intrct:
            for t in intrct["stacking"]:
                if len(t) > 0: s_txt += t + " "
        if "vdw" in intrct:
            for t in intrct["vdw"]:
                if "hbonds" in intrct and t in intrct["hbonds"]: continue
                if "stacking" in intrct and t in intrct["stacking"]: continue
                if len(t) > 0: v_txt += t + " "
        par1 = table.cell(i_row, i_col).add_paragraph(h_txt, style='Normal')
        par1.style.font.color.rgb = RGBColor(0x42, 0x24, 0xE9)
        par2 = table.cell(i_row, i_col).add_paragraph(s_txt, style='Body Text')
        par2.style.font.color.rgb = RGBColor(10, 200, 10)
        par3 = table.cell(i_row, i_col).add_paragraph(v_txt, style='Quote')
        par3.style.font.color.rgb= RGBColor(0, 0, 0)

    i_col += 1
document.save('interactions-pocket-auto.docx')
