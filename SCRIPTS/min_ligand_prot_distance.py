import json
import sys


def minimum_ligand_protein_distance(contact_file):
    d = {}
    with open(contact_file) as f:
        old_tok = ['X', 'lig', 'ligid', 'atom', 'chain', 'res', '0', 'protein', 'atom', 10000]
        for line in f:
            if line[0] is 'X':
                tok = line.split()
                if tok[5] == 'HEM':
                    tok[6] = '0'
                if tok[6] != old_tok[6]:
                    old_tok[6] = tok[6]
                    old_tok[9] = 10000
                elif float(tok[9]) < float(old_tok[9]):
                    d[tok[6]] = tok[9]
                    old_tok = tok
                #print(json.dumps(d))
        return json.dumps(d)


if __name__ == '__main__':
    contact_file = sys.argv[1]
    print(minimum_ligand_protein_distance(contact_file))
