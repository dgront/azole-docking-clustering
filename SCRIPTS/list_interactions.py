import sys,  glob

sys.path.append("/home/dgront/src.git/bioshell/bin/")

from pybioshell.core.data.io import Pdb
from pybioshell.core.calc.structural.interactions import StackingInteractionCollector, HydrogenBondCollector, VdWInteractionCollector
from pybioshell.utils import LogManager, read_two_column_file
from pybioshell.std import vector_std_shared_ptr_core_calc_structural_interactions_ResidueInteraction_t

ligand_code = sys.argv[1]
pdb_fname = sys.argv[2]
LogManager.INFO()

#our_names = ["Clo", "Ket", "Vor", "Itr", "Obt", "Lan", "Flu", "HEM"]
#pdb_names = ["CL6", "KTN", "VOR", "1YN", "DVE", "LAN", "TPN", "HEM"]

# Process a subset only
our_names = ["Clo", "Ket"]
pdb_names = ["CL6", "KTN"]

sta_collector = StackingInteractionCollector()
hyd_collector = HydrogenBondCollector()
vdw_collector = VdWInteractionCollector()

for our, pdb in zip(our_names, pdb_names):
    atom_dict = read_two_column_file(pdb+".names")
    sta_collector.rename_atoms_in_monomer_definition(pdb, our, atom_dict)
    hyd_collector.rename_atoms_in_monomer_definition(pdb, our, atom_dict)


s = Pdb(pdb_fname, "").create_structure(0)
stacking = vector_std_shared_ptr_core_calc_structural_interactions_ResidueInteraction_t()
hbonds = vector_std_shared_ptr_core_calc_structural_interactions_ResidueInteraction_t()
vdw = vector_std_shared_ptr_core_calc_structural_interactions_ResidueInteraction_t()
sta_collector.collect_interactions(s, stacking)
hyd_collector.collect_interactions(s, hbonds)
vdw_collector.collect_interactions(s, vdw)

print("# calculated by",sys.argv[0])
for name, lst in zip(["stacking", "hbonds", "vdw"],[stacking, hbonds, vdw]):
    print(name, ":", end=" ")
    a_set = []
    for i in lst:
        if i.first_residue().residue_type().code3 == ligand_code:
            code = i.second_residue().residue_type().code1
            if code == '?': code = i.second_residue().residue_type().code3
            a_set.append("%s%d" % (code, i.second_residue().id()))
        elif i.second_residue().residue_type().code3 == ligand_code:
            code = i.first_residue().residue_type().code1
            if code == '?': code = i.first_residue().residue_type().code3
            a_set.append("%s%d" % (code, i.first_residue().id()))

    a_set = set(a_set)
    for i in a_set:
        print(i, end=",")
    print()


