#load by_HEM_00040.pdb

set orthoscopic,on
space cmyk
set cartoon_transparency, 0.7
bg_color white
util.cbss("all","salmon", "skyblue","gray",_self=cmd)
select hem, resn hem
color smudge, hem
set_bond stick_radius, 0.3, hem
set sphere_scale, 0.3
show spheres, hem
show sticks, hem

select iron, element fe
alter iron, vdw=3.0
rebuild iron
show spheres, iron
color tv_red, iron

select ligand, organic and not hem
color tv_orange, ligand
color atomic, ligand and not elem C
zoom ligand, 7

ray 2500, 2000
#png all-sup-bioshell.png

