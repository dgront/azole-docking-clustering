import sys,  glob

sys.path.append("/home/dgront/src.git/bioshell/bin/")

from pybioshell.core.data.io import DsspData, create_fasta_secondary_string
from pybioshell.core.data.sequence import count_helices_strands

dssp = DsspData(sys.argv[1], True)
for si in range(dssp.count_chains()):
    sec_str = create_fasta_secondary_string(dssp.sequence(si), 80)
    cnts = count_helices_strands(sec_str)
    print(cnts[0], cnts[1])

