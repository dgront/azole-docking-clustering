INP="../../INPUTS"
for i in `cat $INP/proteins`; 
do 
  cd $i; 
  for j in `cat ../$INP/ligands`; 
  do 
    cd $j; 
    ls
#    ln -s /STORAGE/DATA/jmacnar/Akapo_docking/00040/Clotrimazole/outputs_10000 ./o; 
    python3 ../../../../SCRIPTS/combine_silent_fasc.py ../../../../OUTPUTS/$i/$j/o/$i"_HEM_"$j.pdb.fasc fe-distances.sc
    mv out-2.fsc energy-distance.fsc
    cd ../;
  done; 
  cd ../; 
done

