#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/users/jkrys/src.git/Python-3.7.2/
#for i in 00040; do
for i in `cat proteins`; do
  cd $i
#  for j in Clotrimazole Eburicol; do 
  for j in `cat ../ligands`; do
    cd $j
    ligand=`cat ligand`
    sort -nr clusters-5.00.txt | head -10 > s
    /home/users/jkrys/src.git/Python-3.7.2/python ../../create_clusters_pdb-funk.py s  $ligand "../../"$i"-"$j"-5.0-"
    sort -nr clusters-7.00.txt | head -10 > s
    /home/users/jkrys/src.git/Python-3.7.2/python ../../create_clusters_pdb-funk.py s  $ligand "../../"$i"-"$j"-7.0-"
    rm s
    cd ../
  done
  cd ../
done
