for i in *.pdb; do k=`basename $i .pdb`; ap_stacking_interactions $i ../../../INPUTS/Ligand_structures/Eburicol_ligand.pdb | grep "Ebu" > $k.stacking; done
for i in *.pdb; do k=`basename $i .pdb`; ap_hbonds $i ../../../INPUTS/Ligand_structures/Eburicol_ligand.pdb | grep "Ebu" > $k.hbonds; done
for i in *.pdb; do k=`basename $i .pdb`; ap_vdw_interactions $i  | grep "Ebu" > $k.vdw; done



