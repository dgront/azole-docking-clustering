#!/bin/bash

#PBS -V
#PBS -l walltime=720:00:00
#PBS -o $PBS_O_WORKDIR/stdout
#PBS -e $PBS_O_WORKDIR/stderr
cd $PBS_O_WORKDIR

LIGAND=`cat ligand`

date >log-clustering
~/src.git/bioshell/bin/ap_ligand_clustering $LIGAND catlista 10 5.0 7.0 10.0 >out-clustering 2>>log-clustering
date >>log-clustering



