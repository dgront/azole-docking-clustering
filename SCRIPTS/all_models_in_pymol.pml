load by_HEM_00040.pdb
load by_HEM_1742.pdb
load by_HEM_271374.pdb
load by_HEM_345805.pdb
load by_HEM_367271.pdb
load by_HEM_38441.pdb
load by_HEM_523016.pdb
load by_HEM_627561.pdb
load by_HEM_629036.pdb
load by_HEM_809380.pdb
load by_HEM_EAL23379.pdb
load by_HEM_KIR77383.pdb

load by_HEM_00040.pdb

bg_color white
util.cbss("all","salmon", "skyblue","gray",_self=cmd)
select hem, resn hem
color smudge, hem
set_bond stick_radius, 0.3, hem
set sphere_scale, 0.3

show spheres, hem
show sticks, hem

select iron, element fe
alter iron, vdw=3.0
rebuild iron
show spheres, iron
color tv_red, iron

ray 2500, 2000
png all-sup-bioshell.png

