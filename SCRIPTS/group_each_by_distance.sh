INP="../../INPUTS"
for i in `cat $INP/proteins`; 
do 
  mkdir -p $i
  cd $i; 
  for j in `cat ../$INP/ligands`; 
  do 
    mkdir -p $j
    cd $j; 
#    python3 ../../../../SCRIPTS/ligand_fe_distance.py -p "/mnt/storage2/jmacnar/Akapo_docking/"$i"/"$j"/outputs_10000/*" >stdout 2>stderr
#      python3 /Users/dgront/src.git/bioshell/doc_examples/py-examples/core/data/filter_scorefile/filter_scorefile.py \
#    	    -f energy-distance.fsc -c total_score min_distance min_atom tag > lowres.fsc
#      python3 /Users/dgront/src.git/bioshell/doc_examples/py-examples/core/data/filter_scorefile/filter_scorefile.py\
#	     -f hires_ligand_scores.out -c total min_distance atom_name tag > ligand.fsc    	    
#      python3 /Users/dgront/src.git/bioshell/doc_examples/py-examples/core/data/filter_scorefile/filter_scorefile.py\
#	     -f hires_pose_scores.out -c total min_distance atom_name tag > pose.fsc
#      for f in ligand.fsc lowres.fsc pose.fsc:
#      do
#	curl -O  https://bitbucket.org/dgront/azole-docking-clustering/raw/HEAD/RESULTS/fe-distances-all/$i/$j/$f
#      done
#    d=`coln 18 energy-distance.fsc | sort -nr | head -1`
    d=`sort -nk 18 energy-distance.fsc | head -2 | tail -1`
    echo $i $j $d
    cd ../;
  done; 
  cd ../; 
done

