import sys

#-------------------------------------------------
#
# Run this script in OUTPUTS
#-------------------------------------------------

# Main directory of the project
DIR     = "/home/users/dgront/runs/Akapo-results/azole-docking-clustering/"

sys.path.append(DIR)
import os
from pybioshell.core.data.io import Pdb

OUTPUTS = os.path.join(DIR,"OUTPUTS")
RESULTS = DIR + "/RESULTS/first-10k/"

clusters_file = open(sys.argv[1])
ligand_code = sys.argv[2]
pdb_path = sys.argv[3]
outfile_prefix = sys.argv[4]

pdb_path = os.path.join(OUTPUTS,pdb_path)

iline = 0
for line in clusters_file:
    iline += 1
    tokens = line.strip().split()
    out_fname = RESULTS + outfile_prefix+"c"+str(iline)+'-'+tokens[0]+".pdb"
    print(out_fname,"stores",line.strip())
    outp = open(out_fname,"w")
    p = Pdb(os.path.join(pdb_path,tokens[1]),"is_not_hydrogen", False)
    s = p.create_structure(0)
    i_chain = 0
    for ic in range(s.count_chains()):
        c = s[ic]
        for ir in range(c.count_residues()):
            r = c[ir]
            for ia in range(r.count_atoms()):
                a = r[ia]
                outp.write(a.to_pdb_line() + "\n")

    outp.close()

