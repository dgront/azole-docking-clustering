import sys,  glob

sys.path.append("/home/dgront/src.git/bioshell/bin/")

from pybioshell.core.data.io import Pdb, write_pdb
from pybioshell.core.data.structural import SelectChainResidueAtom, SelectChainResidues
from pybioshell.std import vector_core_data_basic_Vec3
from pybioshell.core.calc.structural.transformations import *
from pybioshell.core.chemical import MonomerStructureFactory
from pybioshell.utils import LogManager

HEM_FILE = "./HEM.cif"

LogManager.INFO()

reference_atoms_im_cif = [" NA ", " NB ", " NC ", " ND "]
reference_atoms_im_model = [" N1 ", " N2 ", " N4 ", " N3 "]

# ---------- Data related to a model
model = Pdb(sys.argv[1], "").create_structure(0)                    # --- Load a model structure - PDB format
hem_model = model.find_residues(SelectChainResidues('B:HEM'))[0]    # --- Find HEM residue
ref_model = vector_core_data_basic_Vec3()                           # --- vector for superimposed coordinates

# ---------- Here we find the four atoms of PDB model HEM that are used as the reference frame
for atom_name in reference_atoms_im_model:
    selector = SelectChainResidueAtom('*','HEM', atom_name)
    found = model.find_atoms(selector)
    if len(found) == 0:
        print("Can't find", atom_name)
    else:
        ref_model.append(found[0])
        print(found[0])

# ---------- Here we find the four atoms of CIF HEM that are used as the reference frame
ref_cif = vector_core_data_basic_Vec3()                             # --- vector for superimposed coordinates
monomers = MonomerStructureFactory.get_instance()                   # --- factory that produces monomers from CIF data
monomers.register_monomer(HEM_FILE)                                 # --- Load the HEM geometry - CIF format
hem_cif = monomers.get("HEM")                                       # --- get HEM
hem_atoms_map = {}                                                  # --- temporary dict of HEM atoms
for i in range(hem_cif.count_atoms()):                              # --- pack atoms to a temporary dict
    a = hem_cif.get_atom(i)
    hem_atoms_map[a.atom_name()] = a
for atom_name in reference_atoms_im_cif:                            # --- get the reference atoms in the right order
    ref_cif.append(hem_atoms_map[atom_name])

# ---------- Find the rototranslation and superimposes one HEM on the other
rms = CrmsdOnVec3()
crmsd_val_1 = rms.crmsd(ref_model, ref_cif, 4, True)

hem_model.clear()
for i in range(hem_cif.count_atoms()):
    ai = hem_cif.get_atom(i)
    rms.apply_inverse(ai)
    hem_model.push_back(ai)

write_pdb(model, "fixed-"+sys.argv[1])

