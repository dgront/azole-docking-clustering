import sys, os
from os.path import expanduser
sys.path.append(os.path.join(expanduser("~"), "src.git/visualife"))

from visualife.data import ScoreFile

sc = ScoreFile(sys.argv[1])
sc.merge_in(ScoreFile(sys.argv[2]))

sc.write(None)