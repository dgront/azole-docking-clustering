#!/bin/bash

#PBS -V
#PBS -l walltime=720:00:00
#PBS -o $PBS_O_WORKDIR/stdout
#PBS -e $PBS_O_WORKDIR/stderr
#cd $PBS_O_WORKDIR

DIR="/home/users/dgront/runs/Akapo-results/azole-docking-clustering"
mpath=`pwd`
ligand=`echo $mpath | tr '/' ' ' | awk '{print $(NF)'}`
protein=`echo $mpath | tr '/' ' ' | awk '{print $(NF-1)'}`


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/users/jkrys/src.git/Python-3.7.2/

fname=$DIR/OUTPUTS/$protein/$ligand/het.pdb
echo $fname 
/home/users/jkrys/src.git/Python-3.7.2/python $DIR/SCRIPTS/ligand_fe_distance.py $fname > $DIR/RESULTS/fe-distances-all/$protein/$ligand/fe-distances 2>>ERR




