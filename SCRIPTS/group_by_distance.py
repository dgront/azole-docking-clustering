import sys, os, json

sys.path.append("/Users/dgront/src.git/visualife/")
from visualife.data import ScoreFile


drugs = ["Clotrimazole", "Eburicol", "Fluconazole", "Itraconazole", "Ketoconazole", "Lanosterol", "Obtusifoliol", "VT_1129", "Voriconazole"]
proteins = ["00040", "1742", "271374", "345805", "367271", "38441", "523016", "627561", "629036", "809380", "EAL23379", "KIR77383"]
results = {}
for drug in drugs[:]:
    for prot in proteins[:]:

        bin_clash = 0       # --- below 1.5
        bin_ok = 0          # --- from 1.5 to 2.5
        bin_ok_near = 0     # --- from 2.5 to 3.5
        bin_pocket = 0      # --- below 8
        bin_away = 0        # --- above 8

        path = os.path.join(prot, drug, "fe-distances.sc")
        if not os.path.isfile(path):
            continue
        sc_file = ScoreFile()
        sc_file.read_score_file(path)
        min_dist = sc_file.column("min_distance")
        min_n_dist = sc_file.column("min_n_distance")

        for i in range(len(min_dist)):
            d = min_n_dist[i] if min_n_dist[i] < 999 else min_dist[i]
            if d < 1.5 : bin_clash += 1
            elif d <= 2.5: bin_ok += 1
            elif d <= 3.5: bin_ok_near += 1
            elif d < 8.0: bin_pocket += 1
            else: bin_away += 1

        results[prot + ':' + drug] = [bin_clash, bin_ok, bin_ok_near, bin_pocket]

print(results)
