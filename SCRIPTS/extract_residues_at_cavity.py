import sys

cavity_file = sys.argv[1]
which_cavity = sys.argv[2]

amino_acid_code3_to_code1 = {'ALA': 'A', 'ARG': 'R', 'ASN': 'N', 'ASP': 'D',
                    'CYS': 'C', 'GLU': 'E', 'GLN': 'Q', 'GLY': 'G',
                    'HIS': 'H', 'ILE': 'I', 'LEU': 'L', 'LYS': 'K',
                    'MET': 'M', 'PHE': 'F', 'PRO': 'P', 'SER': 'S',
                    'THR': 'T', 'TRP': 'W', 'TYR': 'Y', 'VAL': 'V'}

for line in open(cavity_file):
  if line.startswith("Cavity"+which_cavity+"|"):
    aa = line.split('|')[1].split(' ')
    for iaa in aa:
      tokens = iaa.split(':')
      token = amino_acid_code3_to_code1[tokens[0]]+tokens[1]
      print(token, end=",")
print()


