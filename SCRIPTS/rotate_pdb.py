import re, sys

from pybioshell.core.data.io import Pdb
from pybioshell.core.calc.structural.transformations import Rototranslation

pattern = r"rotation matrix.+^0\s+(-?\d+?\.\d*)\s+(-?\d\.\d*)\s+(-?\d\.\d*)\s+(-?\d\.\d*).+^1\s+(-?\d+?\.\d*)\s+(-?\d\.\d*)\s+(-?\d\.\d*)\s+(-?\d\.\d*).+^2\s+(-?\d+?\.\d*)\s+(-?\d\.\d*)\s+(-?\d\.\d*)\s+(-?\d\.\d*)"


def rot_matrix(fname):
    rot_data = open(fname).read()
    m = re.findall(pattern, rot_data, re.MULTILINE | re.DOTALL)
    rt_floats = [float(v) for v in m[0]]
    # print(rt_floats)
    rt = Rototranslation()
    rt.tr_after(rt_floats[0], rt_floats[4], rt_floats[8])
    rt.rot_x(rt_floats[1], rt_floats[5], rt_floats[9])
    rt.rot_y(rt_floats[2], rt_floats[6], rt_floats[10])
    rt.rot_z(rt_floats[3], rt_floats[7], rt_floats[11])
    # print(rt)
    return rt

if __name__ == "__main__":
    rt = rot_matrix(sys.argv[1])
    strctr = Pdb(sys.argv[2], "").create_structure(0)
    for ic in range(strctr.count_chains()):
        c = strctr[ic]
        for ir in range(c.count_residues()):
            r = c[ir]
            for ia in range(r.count_atoms()):
                rt.apply(r[ia])
                print(r[ia].to_pdb_line())
