import sys
sys.path.append("/home/users/dgront/runs/Akapo-results/clustering")

from pybioshell.core.data.io import Pdb

chain_ids = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvw"
prefix = ""
if len(sys.argv) > 3 : prefix = sys.argv[3]
clusters_file = open(sys.argv[1])
ligand_code = sys.argv[2]
iline = 0
for line in clusters_file:
    iline += 1
    tokens = line.strip().split()
    out_fname = prefix+"c"+str(iline)+'-'+tokens[0]+".pdb"
    print(out_fname,"stores",line.strip())
    outp = open(out_fname,"w")
    p = Pdb(tokens[1],"", False)
    s = p.create_structure(0)
    i_chain = 0
    for ic in range(s.count_chains()):
        c = s[ic]
        for ir in range(c.count_residues()):
            r = c[ir]
            for ia in range(r.count_atoms()):
                a = r[ia]
                outp.write(a.to_pdb_line() + "\n")

    for fname in tokens[2:]:
        p = Pdb(fname,"", False)
        s = p.create_structure(0)
        for ic in range(s.count_chains()):
            c = s[ic]
            for ir in range(c.count_residues()):
                r = c[ir]
                if r.residue_type().code3 != ligand_code: continue
                r.owner().id(chain_ids[i_chain])
                for ia in range(r.count_atoms()):
                    a = r[ia]
                    outp.write(a.to_pdb_line() + "\n")
    outp.close()

