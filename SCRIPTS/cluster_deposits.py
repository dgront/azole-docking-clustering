#!/bin/env python

import sys, subprocess, glob


"""
The first argument of the script is the file with distances, its format is:

1EA1.pdb  470 TPF A - 2IJ7.pdb 2472 TPF A :   4.742    4.596
1EA1.pdb  470 TPF A - 2IJ7.pdb 2470 TPF B :   4.719    4.860
1EA1.pdb  470 TPF A - 2IJ7.pdb 2471 TPF C :   4.743    4.796
1EA1.pdb  470 TPF A - 2IJ7.pdb 2473 TPF D :   4.737    4.658
"""

def distances_from_file(fname):

  out = []
  for l in open(fname):
    tokens = l.strip().split()
    t1 = tokens[0]+"-"+tokens[3]+"-"+tokens[1]
    t2 = tokens[5]+"-"+tokens[8]+"-"+tokens[6]
    d = tokens[11]
    out.append( (t1,t2,d) )
    out.append( (t2,t1,d) )

  return out

def run_clustering(fname, n_clusters):

  dist = distances_from_file(fname)
  uniq = set([o[0] for o in dist])
  dist_file=open("dist","w")
  for o in dist:
    dist_file.write("%s %s %s\n"%o)
  dist_file.close()
  
  cmd = "clust -i=%s -n=%d -complete -clustering:missing_distance=10.0  -clustering:out:tree" % ("dist",len(uniq))
  out = subprocess.run(cmd, shell=True, capture_output=True,  text=True)
  s = str(out.stdout)
  print(s)
  s = s.split("\n")
  cutoff = float(s[len(s)-n_clusters-1].strip().split()[5]) + 0.001
  print(cutoff)
  
  cmd = "clust -i=%s -n=%d -complete -clustering:missing_distance=10.0 -clustering:out:clusters -clustering:out:distance=%f" % ("dist",len(uniq), cutoff)
  out = subprocess.run(cmd, shell=True, capture_output=True,  text=True)
  s = str(out.stdout)

  for c in glob.glob("c*_*"):
    lines = open(c).readlines()
    for l in lines:
      if l.startswith("# medoid element"):
        print(l.strip().split()[3])

if __name__ == "__main__":
  
  nc = int(sys.argv[2]) if len(sys.argv) > 2 else 4
  run_clustering(sys.argv[1], nc)
