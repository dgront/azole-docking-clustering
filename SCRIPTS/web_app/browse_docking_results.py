#!/usr/bin/python
# coding: utf-8
import glob, os, logging, sys, json, requests
import urllib.request
from flask import render_template, Flask, jsonify, request
from os.path import expanduser
sys.path.append(os.path.join(expanduser("~"), "src.git/visualife"))

from visualife.serverside.utils import scorefile_to_json_dictionary
from visualife.data import parse_pdb_data, create_sequence, ScoreFile

def pythonanywhere_settings():
    global DIR, data_path, all_distances_path, app_name_prefix, port, is_on_pythonanywhere, models_path
    DIR = "/home/azoledocking/azole_docking_clustering"
    data_path = DIR+"/RESULTS/first-10k/"
    all_distances_path = DIR+"/RESULTS/fe-distances-all"
    app_name_prefix = "/azoledocking/"
    port = 5000
    models_path = None # No models on  pythonanywhere
    is_on_pythonanywhere = True


def bioshell_settings():
    global DIR, data_path, all_distances_path, app_name_prefix, port, is_on_pythonanywhere, models_path
    DIR = "/home/dgront/work.git/azole-docking-clustering/"
    data_path = DIR+"/RESULTS/first-10k/"
    all_distances_path = DIR+"/RESULTS/fe-distances-all"
    app_name_prefix = "/azoledocking"
    port = 5004
    is_on_pythonanywhere = False
    models_path = "/home/dgront/public_html/azole-docking-clustering/OUTPUTS/" #00040/Clotrimazole/outputs_10000/"


def local_settings():
    global DIR, data_path, all_distances_path, app_name_prefix, port, is_on_pythonanywhere, models_path
    DIR = "../.."
    data_path = DIR+"/RESULTS/first-10k/"
    all_distances_path = DIR + "/RESULTS/fe-distances-all"
    models_path = "http://bioshell.pl/~dgront/azole-docking-clustering/OUTPUTS/" #00040/Clotrimazole/outputs_10000/"
    app_name_prefix = "/azoledocking"
    port = 5004
    is_on_pythonanywhere = False


# pythonanywhere_settings()         # ---------- run pythonanywhere version as the default
local_settings()                    # ---------- run the local version of the app
# bioshell_settings()			    #---------- run the app on bioshell.pl

app = Flask(__name__, static_url_path=app_name_prefix)
app.logger.setLevel(logging.INFO)
if_loaded = False


# -------------------- Flask stuff - necessary to serve the data online ---------------
@app.route(app_name_prefix+'/', methods=['GET', 'POST'])
def entry_point():
    global if_loaded
    if_loaded = False
    return render_template('browse_docking_results.html')


@app.route(app_name_prefix+'/browse_docking_results-bry.py', methods=['GET', 'POST'])
def brython_script():
    return render_template('browse_docking_results-bry.py')


@app.route(app_name_prefix+'/get_pdb/', methods=['POST'])
def get_pdb():

    if "key" not in request.form :
        app.logger.error("client requested a PDB data but >key< key not found in request")
        return jsonify({"status": "Failed"})

    fkey = request.form["key"]
    if fkey.find("pdb") == -1 :     # --- the case when client asks for a cluster medoid PDB
        path = os.path.join(data_path, files[fkey])
    else:                           # --- the case when just a model should be sent
        tokens = fkey.replace("VT_1129", "VT-1129").replace('_', '.').split('.')
        protein, drug = tokens[0], tokens[2].replace("VT-1129", "VT_1129")
        path = os.path.join(models_path, protein, drug, "outputs_10000", fkey)

    if path.find('http') >= 0 :     # --- The case when models are remotely stored on bioshel.pl
        app.logger.info("client requested a PDB by key: %s, remote content from %s returned" % (fkey, path))
        r = requests.get(path, allow_redirects=True)
        pdb_as_text = r.content.decode("utf-8")
        strctr = parse_pdb_data(pdb_as_text, parse_atoms=False)[0]      # --- we need only its sequence
        seq = create_sequence(strctr.chains[0].residues)

        # --- now we try to get JSON with contacts
        path = os.path.join(models_path, protein, drug, "outputs_10000/contacts_" + fkey+".json")
        r = requests.get(path, allow_redirects=True)
        contacts = json.loads(r.content.decode("utf-8"))
        resp = {"pdb_data": pdb_as_text, "status": "OK", "key": fkey, "seq": seq, "contacts": contacts}
        return jsonify(resp)
    else:
        if not os.path.isfile(path):
            app.logger.error("can't provide PDB data for:", fkey)
            return jsonify({"status": "Failed"})
        pdb_as_text = open(path).read()
        strctr = parse_pdb_data(pdb_as_text, parse_atoms=False)[0]      # --- we need only its sequence
        seq = create_sequence(strctr.chains[0].residues)
        app.logger.info("client requested a PDB by key: %s, file %s returned" % (fkey, fkey))
        return jsonify({"status": "OK", "pdb_data": pdb_as_text, "key": fkey, "seq": seq})


@app.route(app_name_prefix+'/get_lists/', methods=['POST'])
def get_lists():
    global if_loaded
    if not if_loaded:
        load_pdbs_names(DIR+"/RESULTS/first-10k/")
        load_distances(DIR+"/RESULTS/fe-distances")
        if_loaded = True

    by_distance = open(DIR+"/RESULTS/fe-distances-all/groups_by_distance.json").read()
    by_distance = by_distance.replace("'", "\"")

    return jsonify({"drugs": drugs, "proteins": proteins, "cutoffs": cutoffs, "sizes": sizes,
                    "distances": distances, "by_distance": json.loads(by_distance)})


def distance_bin(d):
    """Filter for a score file - to get its rows relevant to a given bar """
    if d < 1.5: return 0
    elif d <= 2.5: return 1
    elif d <= 3.5: return 2
    elif d < 8.0: return 3
    else: return 4


@app.route(app_name_prefix+'/get_scores_distances/', methods=['POST'])
def get_scores_distances():

    if "drug" not in request.form or "protein" not in request.form:
        app.logger.error("client requested score file data but either >protein< or >drug< key not found in request")
        print(request.form)
        return jsonify({"status": "Failed"})

    drug = request.form["drug"]
    prot = request.form["protein"]
    if 'bin' in request.form:
        which_bin = int(request.form["bin"])

        def filter(val):
            if distance_bin(val) == which_bin: return True
            return False
    else:
        filter = None

    what_scores = request.form.get("scorefile", "pose") # --- available scores: "pose", "ligand", "lowres"
    if what_scores == "lowres":
        fname = os.path.join(all_distances_path, prot, drug, "lowres.fsc")
        app.logger.info("Sending: %s %s - %s" % (drug, prot, fname))
        return jsonify(scorefile_to_json_dictionary(fname, send_columns=False, filter=filter, filter_by="min_distance",
                                                selected_columns=["min_distance", "min_atom", "total_score", "tag"]))
    elif what_scores == "pose":
        sc_fname = os.path.join(all_distances_path, prot, drug, "pose.fsc")
        hb_fname = os.path.join(all_distances_path, prot, drug, "contacts_hbonds.sc")
        app.logger.info("Sending: %s %s - %s" % (drug, prot, sc_fname))
        sc = ScoreFile(sc_fname)
        column_names = ["min_distance", "atom_name", "total", "tag"]
        if os.path.isfile(hb_fname):
            app.logger.info("Merging %s" % hb_fname)
            sc.merge_in(ScoreFile(hb_fname))
            column_names = ["contacts", "hbonds"] + column_names
        else:
            app.logger.warning("can't find file: %s" % hb_fname)
        return jsonify(scorefile_to_json_dictionary(sc, send_columns=False, filter=filter, filter_by="min_distance",
                                                    selected_columns=column_names))
    elif what_scores == "ligand":
        fname = os.path.join(all_distances_path, prot, drug, "ligand.fsc")
        app.logger.info("Sending: %s %s - %s" % (drug, prot, fname))
        return jsonify(scorefile_to_json_dictionary(fname, send_columns=False, filter=filter, filter_by="min_distance",
                                                    selected_columns=["min_distance", "atom_name", "total", "tag"]))
    else:
        return jsonify({"status": "Failed"})


# -------------------- data and routines specific to P450 docking project ---------------
drugs = ["Clotrimazole", "Eburicol", "Fluconazole", "Itraconazole", "Ketoconazole", "Lanosterol", "Obtusifoliol", "VT_1129", "Voriconazole"]
proteins = ["00040", "1742", "271374", "345805", "367271", "38441", "523016", "627561", "629036", "809380", "EAL23379", "KIR77383"]
cutoffs = ["7.0", "5.0"]
files = {}
sizes = {}
distances = {}
local_reference_frames = {}


def pdb_name_to_key_size(pdb_file_name):
    fname = pdb_file_name.split("/")[-1]
    tokens = fname.split("-")  # e.g. 629036-Clotrimazole-5.0-first-c10-11.pdb
    return tokens[0] + tokens[1] + tokens[2] + tokens[4], int(tokens[5].split(".")[0])


def load_pdbs_names(data_path, cutoff="5.0"):
    """Loads PDB files that are the output of the ligand clustering procedure.

    Every PDB file is named like ``629036-Clotrimazole-5.0-first-c10-11.pdb`` where:
      - ``5.0`` is the clustering cutoff value
      - ``c10-11`` cluster ID (10 is cluster index, 11 is its size)
      - ``first`` means first structure in a cluster
      - ``629036`` is the sequence ID
      - ``Clotrimazole``is the ligand name
    :param data_path: path where to look for ligand (using ``glob()``)
    :param cutoff: clustering cutoff, here "7.0" or "5.0"
    :return: None, data is loaded to ``sizes`` and ``files`` global lists
    """
    for fname in glob.glob(os.path.join(data_path, "*"+cutoff+"*first*.pdb")):
        key, size = pdb_name_to_key_size(fname)
        sizes[key] = size
        files[key] = fname.split("/")[-1]
    app.logger.info(str(len(files)) +" pdb clusters registered")


def load_distances(filename):
    """ Loads an ``fe-distances`` file.

    The ``fe-distances`` file has 2 columns: structure ID, e.g. ``00040Clotrimazole5.0c1`` and the  distance between
    a ligand and FE atom

    :param filename: the input file (use ``fe-distances`` with a proper path)
    :return: None
    """
    for l in open(filename):
        tokens = l.strip().split()
        distances[tokens[0]] = float(tokens[1])


if __name__ == '__main__':

#    local_settings()                # ---------- run the local version of the app

    app.logger.info("data path is " + DIR)
    app.logger.info(str(len(files)) + "files registered")
    app.run(debug=True, port=port)
