import sys
from javascript import JSON
from browser import window, document, html

sys.path.append("static/")
from visualife.widget import GLViewerWidget, AjaxSender, TooltipWidget, TableWidget, SequenceViewer
from visualife.core.styles import *
from visualife.calc import Histogram
from visualife.core import Plot, HtmlViewport

# --------- Global settings
min_size_offset = 9     # --- subtracted from a cluster size to make a bigger difference between circles

active_models = {}
colors_backup = {}
circle_to_stucture = []
sizes = []              # --- dictionary that holds the size for every cluster
distances = []          # --- dictionary that holds minimal ligand-Fe distance for every cluster
data_as_json = ""       # --- Data coming from the server
n_proteins = 0          # --- the number of proteins (orthologs) used in docking
n_drugs = 0             # --- the number of drugs used in docking
pts = []                # --- array of data to plot by bubble chart: a list of X, Y, Z for every cluster
bars = []               # --- array of data to plot by bar chart: a list of X, height for every bar
bar_tips = []           # --- a tooltip for each bar of the big barplot
pts_distances = []      # --- a list that holds minimal ligand-Fe distance for every cluster
ajax_data = None        # --- data to make plots - received from server
score_table = None      # --- table that contains scorefile data
selected_points = {}    # --- selected points of a scatterplot and their original colors
recent_protein = None   # --- which protein's score file  was plotted most recently?
recent_drug  = None     # --- which drug's score file  was plotted most recently?
histogram = None        # --- histogram object: holds the histogram but not its plot
summary_by_protein = None      # --- bins summed up by protein
summary_by_drug = None         # --- bins summed up by drug

# --- name of the score file columns that are used as X and Y data by the plot
x_axis_column_name = 'min_distance'
y_axis_column_name = 'total_score'
# --- names of 'by-distance' bins
bydistance_names = ["clashes","correct", "near", "pocket"]
pix_x, pix_y = 60, 60   # --- width and height of each square where bubbles are drawn
border_x, border_y = 20, 20


def switch_tab(evt):
    tab_id = evt.target.id.split('-')[0]
    for tab in document.select('.tabcontent') :
        tab.style.display = "none"
    for tab in document.select('.tabbuttons') :
        tab.classList.remove("active")
    document[tab_id].style.display = "block"
    evt.currentTarget.classList.add("active")

def switch_left_tab(evt):
    tab_id = evt.target.id.split('-')[0]
    for tab in document.select('.box') :
        tab.style.display = "none"
    for tab in document.select('.left_tabbuttons') :
        tab.classList.remove("active")
    document[tab_id].style.display = "block"
    evt.currentTarget.classList.add("active")


def display_file(evt):
    ret = JSON.parse(evt.text)
    pdb_as_text = ret["pdb_data"]
    fkey = ret["key"]
    seq = ret["seq"]
    active_models[fkey] = viewer_widget.add_model(fkey, pdb_as_text)
    sequence_viewer.delete_regions()
    sequence_viewer.load_sequence(seq)
    contacts = ret["contacts"]
    # --- 7.0A is the maximum contact distance in this experiment; we put 8 here to go beyond the color map and get white color
    colorz = [8.0 for i in range(len(seq))]
    for pos, distance in contacts.items():
        pos, distance = int(pos), float(distance)
        if pos > 0:
            colorz[pos] = distance
    sequence_viewer.add_to_region("ligand", first_pos=1, last_pos=len(seq), color=colorz,
                                  tooltip="residues closer to the ligand than 7A")
    sequence_viewer.show_region("ligand")


def request_pdb_file(evt):
    get_pdb(file_name=evt.target.id)


def protein_key(protein_name, drug_name, cluster_id, cutoff="5.0"):
    return protein_name + drug_name + cutoff+"c" + str(cluster_id)


def click_on_circle(evt):

    i = int(evt.target.id.split(":")[1])
    pd = circle_to_stucture[i]
    fkey = protein_key(pd[0], pd[1], pd[2])
    if fkey in active_models:
        viewer_widget.remove_model(fkey)
        evt.target.style["fill"] = colors_backup[fkey]
        evt.target.classList.remove("shadow")
        del(active_models[fkey])
    else:
        colors_backup[fkey] = evt.target.style["fill"]
        get_pdb(key=fkey)
        evt.target.style["fill"] = "#50ff1e"


def mouse_over_circle(evt):

    global sizes, distances

    i = int(evt.target.id.split(":")[1])
    pd = circle_to_stucture[i]
    fkey = protein_key(pd[0], pd[1], pd[2])
    pos = evt.target.getBoundingClientRect()
    tip="""<table>
    <tr><td class="tip_left">protein:</td><td class="tip_right">%s</td></tr>
    <tr><td class="tip_left">drug:</td><td class="tip_right">%s</td></tr>
    <tr><td class="tip_left">cluster:</td><td class="tip_right">%d</td></tr>
    <tr><td class="tip_left">size:</td><td class="tip_right">%d</td></tr>
    <tr><td class="tip_left">distance:</td><td class="tip_right">%.2f</td></tr>
</table>
<p><em style="color:white; text-align: left; text-size:15px">click on the circle to open protein structure in the 3D panel</em></p>""" \
        % (pd[0],pd[1],pd[2], sizes[fkey], distances[fkey])
    circle_tip.tooltip_text = tip
    circle_tip.show(int(pos.x), int(pos.y))


def drug_protein_from_mouse_point(evt):

    rect = evt.target.getBoundingClientRect()
    x = evt.clientX - rect.left
    y = evt.clientY - rect.top
    if x < border_x or y < border_y: return None, None
    x = (x - border_x) // pix_x
    y = (y - border_y) // pix_y
    y = 12 - y - 1
    if x < 0 or y < 0 or x >= len(data_as_json["drugs"]) or y >= len(data_as_json["proteins"]): return None, None

    drug = data_as_json["drugs"][x]
    prot = data_as_json["proteins"][y]
    return drug, prot


def scorefile_type_from_radiobuttons():

    if document["lowres"].checked: return "lowres"
    elif document["ligand"].checked: return "ligand"
    return "pose"


def data_to_plot_from_radiobuttons():

    if document["all"].checked: return "all"
    elif document["top_100"].checked: return "top_100"
    elif document["top_1000"].checked: return "top_1000"
    return "current_page"


def plot_type_from_radiobuttons():

    if document["distance_histogram"].checked: return "distance_histogram"
    elif document["score_histogram"].checked: return "score_histogram"
    return "X-Y"


def mouse_over_square(evt):

    global sizes, distances
    drug, protein = drug_protein_from_mouse_point(evt)
    if protein and drug:
        tip="""%s versus %s
    <p><em style="color:grey; text-align: left; text-size:15px">click here to see models for this case</em></p>""" \
            % (drug, protein)
        square_tooltip.tooltip_text = tip
        square_tooltip.show(evt.clientX, evt.clientY)
    else:
        square_tooltip.hide()


def mouse_over_bydistance_bar(evt):

    index = int(evt.target.id.split(':')[-1])
    which = bydistance_names[index % 4]
    p, d, cnt = bar_tips[index]
    tip = """<table>
        <tr><td class="tip_left">bin:</td><td class="tip_right">%s</td></tr>
        <tr><td class="tip_left">protein:</td><td class="tip_right">%s</td></tr>
        <tr><td class="tip_left">drug:</td><td class="tip_right">%s</td></tr>
        <tr><td class="tip_left">counts:</td><td class="tip_right">%d</td></tr>
    </table>
    <p><em style="color:white; text-align: left; text-size:15px">click on the bar to open the list of structures</em></p>""" \
          % (which, p, d, cnt)
    pos = evt.target.getBoundingClientRect()
    bydistance_tip.tooltip_text = tip
    bydistance_tip.show(int(pos.x), int(pos.y))


def mouse_over_histogram_bar(evt):

    index = int(evt.target.id.split(':')[-1])
    x_from, x_to = histogram.bin_from_to(index)
    cnt = int(histogram.counts(index))
    tip="""<p><em style="color:grey; text-align: left; text-size:15px">from %.1f to %.1f: %d counts</em></p>""" \
        % (x_from, x_to, cnt)
    pos = evt.target.getBoundingClientRect()
    square_tooltip.tooltip_text = tip
    square_tooltip.show(int(pos.x), int(pos.y))


def click_for_scorefile_data(evt):
    global recent_protein, recent_drug

    if evt.target.id == "svg":
        recent_drug, recent_protein = drug_protein_from_mouse_point(evt)
        if recent_protein and recent_drug:
            document["modal"].style.visibility = 'visible'
            get_scorefile({'drug': recent_drug, 'protein': recent_protein, 'scorefile': scorefile_type_from_radiobuttons()})
    elif evt.target.tagName == "INPUT":
        get_scorefile({'drug': recent_drug, 'protein': recent_protein, 'scorefile': scorefile_type_from_radiobuttons()})
    elif evt.target.tagName == "rect":
        index = int(evt.target.id.split(':')[-1])
        recent_protein, recent_drug, _ = bar_tips[index]
        which = index % 4
        document["modal"].style.visibility = 'visible'
        get_scorefile({'drug': recent_drug, 'protein': recent_protein, 'scorefile': 'pose', 'bin': which})


def click_on_scatterplot_point_or_table_row(evt):
    global score_table
    print("ID ", evt.target.id)

    if evt.target.id.find("scorefile_plot") >= 0:
        which_point = int(evt.target.id.split(':')[-1])
        which_row = which_point
        print("Click event on scatterplot point:", evt.target.id,  which_point, which_row)

    elif evt.target.id.find("TD") >= 0:
        which_row = int(evt.target.id.split('-')[-2])
        which_point = which_row - score_table.rows_per_page * (score_table.current_page-1)
        print("Click event on table cell:", evt.target.id, which_point, which_row)


    if which_point in selected_points:
        score_table.select_row(which_row, False)
        document["scorefile_plot:"+str(which_point)]["fill"] = selected_points[which_point]
        del(selected_points[which_point])
        viewer_widget.remove_model(score_table.data[which_point][-1])
    else:
        selected_points[which_point] = document["scorefile_plot:"+str(which_point)]["fill"]
        score_table.select_row(which_row, True)
        document["scorefile_plot:"+str(which_point)]["fill"] = 'orange'
        print("loading",score_table.data[which_point][-1])
        # --- load a respective PDB structure to the 3D panel
        get_pdb(key=score_table.data[which_point][-1])




def plot_given_columns(evt=None):
    global drawing, ajax_data, x_axis_column_name, y_axis_column_name, score_table, histogram

    drawing.text_length("ala ma kota")

    which_points = data_to_plot_from_radiobuttons()
    if which_points == "top_100":
        x_column_data = score_table.get_column_data(x_axis_column_name, 0)[:100]
        y_column_data = score_table.get_column_data(y_axis_column_name, 0)[:100]
    elif which_points == "top_1000":
        x_column_data = score_table.get_column_data(x_axis_column_name, 0)[:1000]
        y_column_data = score_table.get_column_data(y_axis_column_name, 0)[:1000]
    elif which_points == "all":
        x_column_data = score_table.get_column_data(x_axis_column_name, 0)
        y_column_data = score_table.get_column_data(y_axis_column_name, 0)
    else:
        x_column_data = score_table.get_column_data(x_axis_column_name, score_table.current_page)
        y_column_data = score_table.get_column_data(y_axis_column_name, score_table.current_page)
    drawing.clear()

    what_plot = plot_type_from_radiobuttons()
    if what_plot == "X-Y":
        plot = Plot(drawing, 80, 680, 25, 230, min(x_column_data) - 1, max(x_column_data) + 1, min(y_column_data) - 1,
                    max(y_column_data) + 1, axes_definition="UBLR")

        plot.scatter(x_column_data, y_column_data, color=0, stroke="darker", markersize=4, fill_opacity=0.3,title="scorefile_plot")

        plot.axes["B"].label = x_axis_column_name
        plot.axes["L"].label = y_axis_column_name
        plot.axes["R"].tics_vertical = False
        plot.axes["L"].tics_vertical = False
        plot.axes["U"].show_tics_labels = False
        plot.axes["R"].show_tics_labels = False

        
        #     drawing.bind(i.id,"mouseout",ttw.hide)
    elif what_plot == "distance_histogram":
        histogram = Histogram(range=(0.5, 85.5), width=1.0)
        histogram.observe(x_column_data)
        plot = Plot(drawing, 50, 650, 25, 230, 0.0, 90, 0, histogram.highest_bin()[1], axes_definition="UBLR")
        hx, hy = [], []
        for i in range(histogram.n_bins):
            hy.append(histogram.counts(i))
            hx.append(histogram.bin_from_to(i)[0])
        plot.bars(hx, hy, width=0.8*histogram.width)
        plot.axes["B"].label = x_axis_column_name
        plot.axes["L"].label = "counts"
    else:
        left = min(y_column_data)
        right = max(y_column_data)
        histogram = Histogram(range=(left,right), n_bins=50)
        histogram.observe(y_column_data)
        plot = Plot(drawing, 50, 650, 25, 230, left, right, 0, histogram.highest_bin()[1], axes_definition="UBLR")
        hx, hy = [], []
        for i in range(histogram.n_bins):
            hy.append(histogram.counts(i))
            hx.append(histogram.bin_from_to(i)[0])
        plot.bars(hx, hy, width=0.8*histogram.width)
        plot.axes["B"].label = y_axis_column_name
        plot.axes["L"].label = "counts"

    for i in plot.axes.keys():
        plot.axes[i].tics_label_font_size = 12
        plot.axes[i].label_font_size = 14
    plot.draw(grid=True)
    circles = document["scorefile_plot"].select("circle")
    for i in circles:
        drawing.bind(i.id, "click", click_on_scatterplot_point_or_table_row)
        drawing.bind(i.id,"mouseover", mouseover_scatterpoint)
    drawing.close()

    if "BarsGroup" in document:
        for bar in document["BarsGroup"].children:
            bar.bind("mouseover", mouse_over_histogram_bar)
            bar.bind("mouseout", square_tooltip.hide)


def mouseover_scatterpoint(evt):
    which_point = int(evt.target.id.split(':')[-1])
    print(which_point)
    print(score_table.data[which_point], recent_protein, recent_drug)


def process_scorefile(evt) :
    global ajax_data, score_table, x_axis_column_name, y_axis_column_name

    ajax_data = JSON.parse(evt.text)
    columns = []
    for i in ajax_data["column_names"]:
        print(i)
        if i == 'tag' or i == 'min_atom':
            columns.append({'title': i, 'field_id': i, 'sorter':'string'})
        else:
            columns.append({'title': i, 'field_id': i, 'format': '%5.2f', 'sorter':'float'})
    document["table"].clear()
    score_table = TableWidget("scorefile", "table", width=700, height=200, rows_per_page=50,
                              columns=columns, data=ajax_data["rows"])
    score_table.add_sort_callback(plot_given_columns)
    score_table.add_page_callback(plot_given_columns)
    score_table.add_cell_callback(click_on_scatterplot_point_or_table_row)
    plot_given_columns()


def clear_all_circles():
    """Clears all circles, changing their fill back to lightskyblue"""
    for c in document['svg'].select('circle'):
        c.style["fill"] = "lightskyblue"


def show_barchart_summary_tables(evt = None):

    global summary_by_protein, summary_by_drug
    by_distance = data_as_json["by_distance"]

    # ---------- Lazy evaluation of summary statistics
    if not summary_by_drug:
        summary_by_protein = [[None, 0, 0, 0, 0] for _ in range(n_proteins)]
        summary_by_drug = [[None, 0, 0, 0, 0] for _ in range(n_drugs)]
        ip = 0
        for p in data_as_json["proteins"]:
            id = 0
            for d in data_as_json["drugs"]:
                key = p + ':' + d
                for i_bar in range(4):
                    summary_by_protein[ip][i_bar+1] += by_distance[key][i_bar]
                    summary_by_drug[id][i_bar+1] += by_distance[key][i_bar]
                id += 1
            ip += 1
        ip = 0
        total = ["average",0, 0, 0, 0]
        for p in data_as_json["proteins"]:
            summary_by_protein[ip][0] = p
            for i_bar in range(1, 5): total[i_bar] += summary_by_protein[ip][i_bar]
            ip += 1
        for i_bar in range(1, 5): total[i_bar] = total[i_bar] /float(n_proteins)
        summary_by_protein.append(total)
        total = ["average",0, 0, 0, 0]
        id = 0
        for d in data_as_json["drugs"]:
            summary_by_drug[id][0] = d
            for i_bar in range(1, 5): total[i_bar] += summary_by_drug[id][i_bar]
            id += 1
        for i_bar in range(1, 5): total[i_bar] = total[i_bar] /float(n_drugs)
        summary_by_drug.append(total)

    document["table"].clear()
    protein_columns = [{'title': "protein", 'field_id': "drug", 'format': '%s', 'sorter': 'string'}]
    for col_name in ["clash", "correct", "near", "pocket"]:
        protein_columns.append({'title': col_name, 'field_id': col_name, 'format': '%d', 'sorter': 'int'})
    protein_summary_table = TableWidget("protein-summary", "protein-summary-table", width=500, height=320, rows_per_page=13,
                              columns=protein_columns, data=summary_by_protein)
    drug_columns = [{'title': "drug", 'field_id': "drug", 'format': '%s', 'sorter': 'string'}]
    for col_name in ["clash", "correct", "near", "pocket"]:
        drug_columns.append({'title': col_name, 'field_id': col_name, 'format': '%d', 'sorter': 'int'})
    drug_summary_table = TableWidget("drug-summary", "drug-summary-table", width=500, height=260, rows_per_page=10,
                              columns=drug_columns, data=summary_by_drug)
    document["summary-modal"].style.visibility = 'visible'


def process_json(evt):

    global data_as_json, sizes, distances, n_proteins, n_drugs, pts, pts_distances

    # ----------  coordinates of the 5 circles representing 5 clusters, relative to a corner of a square
    point_layout = [(0.5, 0.5), (0.25, 0.25), (0.75, 0.25), (0.25, 0.75), (0.75, 0.75)]

    data_as_json = JSON.parse(evt.text)
    sizes = data_as_json["sizes"]
    distances = data_as_json["distances"]
    n_proteins = len(data_as_json["proteins"])
    n_drugs = len(data_as_json["drugs"])
    by_distance = data_as_json["by_distance"]
    pts = []
    pts_distances = []
    ix, iy, iz = 0, 0, 0
    max_y = pix_y * n_proteins

    for p in data_as_json["proteins"]:
        ix = 0
        for d in data_as_json["drugs"]:
            ip = 0
            for pl in point_layout:
                ip += 1
                try:
                    key = protein_key(p, d, ip)
                    size = sizes[key]
                    dist = float(distances[key])
                    pts.append((ix * pix_x + pix_x * pl[0], iy * pix_y + pix_y * pl[1], size - min_size_offset))
                    pts_distances.append(dist)
                    circle_to_stucture.append((p, d, ip))
                except:
                    print("key not found:",protein_key(p, d, ip))
            key = p + ':' + d
            for i_bar in range(4):
                h = min(by_distance[key][i_bar], 55)
                bars.append( ((ix+0.166*i_bar) * pix_x + border_x + 10, max_y - iy * pix_y + border_y - h - 2, h))
                bar_tips.append((p, d, by_distance[key][i_bar]))
            ix += 1
        iy += 1

    build_results_bubblechart(None)
    build_results_barchart(None)


def build_resultsplot_frame(drawing_viewport):

    max_y = pix_y * n_proteins
    max_x = pix_x * n_drugs

    stroke_color = color_by_name("SteelBlue").create_darker(0.3)
    pl = Plot(drawing_viewport, border_x, max_x + border_x, border_y, max_y + border_y, 0.0, max_x, 0.0, max_y,
              axes_definition="UBLR")
    for l in ["B","U"]: pl.axes[l].tics(0, 10)
    for l in ["L","R"]: pl.axes[l].tics(0, 13)
    for ax in pl.axes.values():
        ax.are_tics_inside = False
        ax.fill = stroke_color
        ax.stroke = stroke_color
        ax.stroke_width = 3.0
        ax.show_tics_labels = False

    # ---------- horizontal labels: drug names
    ix = 0
    for d in data_as_json["drugs"]:
        pl.add_extra_label(d, (ix+0.5)*pix_x, pix_y*n_proteins+10)
        pl.add_extra_label(d, (ix + 0.5) * pix_x, -10)
        ix += 1
    # ---------- vertical labels: protein names
    iy = 0
    for d in data_as_json["proteins"]:
        pl.add_extra_label(d, -10, (iy+0.5)*pix_y, angle=-90)
        pl.add_extra_label(d, pix_x*n_drugs+10, (iy+0.5)*pix_y, angle=-90)
        iy += 1

    return pl


def build_results_barchart(evt):
    max_y = pix_y * n_proteins
    max_x = pix_x * n_drugs
    document['svg-1'].width = max_x + border_x * 2
    document['svg-1'].height = max_y + border_y * 2
    drawing = HtmlViewport(document['svg-1'], 0, 0, max_x + border_x * 2, max_y + border_x * 2,
                           color='transparent', download_button=True)
    pl = build_resultsplot_frame(drawing)
    pl.draw(grid=True)

    i_bar = 0
    for bar in bars:
        drawing.rect("bigbar:"+str(i_bar), bar[0], bar[1], 6, bar[2],
                     fill=known_color_scales['tableau10'][i_bar%4], stroke="darker")
        i_bar += 1
    drawing.close()
    for i_bar in range(len(bars)):
        document["bigbar:"+str(i_bar)].bind("mouseover", mouse_over_bydistance_bar)
        document["bigbar:"+str(i_bar)].bind("mouseout", bydistance_tip.hide)
        document["bigbar:"+str(i_bar)].bind("click", click_for_scorefile_data)


def build_results_bubblechart(evt):

    global pts, pts_distances

    max_y = pix_y * n_proteins
    max_x = pix_x * n_drugs
    document['svg'].width = max_x + border_x * 2
    document['svg'].height = max_y + border_y * 2
    drawing = HtmlViewport(document['svg'], 0, 0, max_x + border_x * 2, max_y + border_x * 2,
                           color='transparent', download_button=True)

    pl = build_resultsplot_frame(drawing)

    if_reversed = True
    max_distance = float(document["max_color"].value)
    palette = colormap_by_name(document["palette"].value, 0.0, max_distance, if_reversed)
    palette.right_color = "white"
    document["max_color_value"].innerHTML = document["max_color"].value
    pl.bubbles(pts, markersize=2.5, colors=pts_distances, title="serie", stroke="darker", cmap=palette)
    for i in range(len(circle_to_stucture)):
        drawing.bind("serie:" + str(i), "click", click_on_circle)
        drawing.bind("serie:" + str(i), "mouseover", mouse_over_circle)
        drawing.bind("serie:" + str(i), "mouseout", circle_tip.hide)
    document['svg'].bind("click", click_for_scorefile_data)
    document['svg'].bind("mouseover", mouse_over_square)
    document['svg'].bind("mouseout", square_tooltip.hide)
    pl.draw(grid=True)
    drawing.close()


# ---------- Create menu to select a color palette and bind an action to it
for palette_name in continuous_palettes:
    document["palette"] <= html.OPTION(palette_name)
document["palette"].value = "pinks"
document["palette"].bind("change", build_results_bubblechart)
document["max_color"].bind("change", build_results_bubblechart)

# ---------- Set up the big modal window for a score file table and a scatter plot
drawing = HtmlViewport(document['plot00'], 0, 0, 700, 300, color="white")
document["modal-close"].bind("click", lambda evt: setattr(document["modal"].style, 'visibility', 'hidden'))
for radio in ["ligand", "pose", "lowres"]:
    document[radio].bind("change", click_for_scorefile_data)
for radio in ["top_100", "top_1000", "all", "current_page"]:
    document[radio].bind("change", plot_given_columns)
for radio in ["X-Y", "score_histogram", "distance_histogram"]:
    document[radio].bind("change", plot_given_columns)

# ---------- Create a tooltip for a circle to show info about a cluster
circle_tooltip_style = {"backgroundColor": "black", "color": "#fff", "textAlign": "center", "padding": "5px 0",
                 "borderRadius": "6px", "zIndex": "100000", "marginRight": "auto" }
circle_tip = TooltipWidget("circle_tooltip", "tooltip", "", 200, 180, position="absolute",
                           Class="tooltip", style=circle_tooltip_style)
square_tooltip_style = {"backgroundColor": "white", "color": "gray", "textAlign": "center", "padding": "5px 0",
                 "borderRadius": "6px", "zIndex": "100000", "marginRight": "auto" }
square_tooltip = TooltipWidget("square_tooltip", "tooltip", "", 200, 80, position="absolute",
                           Class="tooltip", style=square_tooltip_style)
bydistance_tooltip_style = {"backgroundColor": "black", "color": "#fff", "textAlign": "center", "padding": "5px 0",
                 "borderRadius": "6px", "zIndex": "100000", "marginRight": "auto","textSize": "0.8em" }
bydistance_tip = TooltipWidget("bydistance_tooltip", "tooltip", "", 200, 120, position="absolute",
                           Class="tooltip", style=bydistance_tooltip_style)

# ---------- Create a 3D molecular viewer widget, register a callback co clear circles when the widget is cleared
document["structure_box"].style.display = "block" # this line here is necesary to GLViewer to have correct size, must be before GLViewer constructor
viewer_widget = GLViewerWidget("viewer_box")
# viewer_widget.add_on_clear(clear_all_circles)
# ---------- Create a sequence viewer widget that is loaded on another tab, next to the 3D viewer
seq_cmap = colormap_by_name("blues", 0.0, 7.0, True)
seq_cmap.right_color = "white"
sequence_viewer = SequenceViewer("seqviewer_box",region_cmap=seq_cmap)

# ---------- Create an AJAX sender and ask for list of drugs and proteins
get_lists = AjaxSender("{{url_for('get_lists')}}")
get_lists.on_complete(process_json)
get_lists()

# ---------- Create an AJAX sender that will be used to retrieve PDB files
get_pdb = AjaxSender("{{url_for('get_pdb')}}")
get_pdb.on_complete(display_file)

# ---------- Create an AJAX sender that will be used to retrieve score files
get_scorefile = AjaxSender("{{url_for('get_scores_distances')}}")
get_scorefile.on_complete(process_scorefile)

# ---------- tab navigation
document["by_cluster"].style.display = "block"
document["by_cluster-button"].classList.add("active")
document["by_cluster-button"].bind("click", switch_tab)
document["by_distance-button"].bind("click", switch_tab)

# ---------- left tab navigation
document["structure_box"].style.display = "block"
document["structure_box-button"].classList.add("active")
document["structure_box-button"].bind("click", switch_left_tab)
document["sequence_box-button"].bind("click", switch_left_tab)

# ---------- buttons to compute summary statistics for quality bins
document["bar-summary-chart"].bind("click", show_barchart_summary_tables)
document["summary-close"].bind("click", lambda evt: setattr(document["summary-modal"].style, 'visibility', 'hidden'))
