import sys,  glob

sys.path.append("/home/dgront/src.git/bioshell/bin/")

from pybioshell.core.data.io import Pdb
from pybioshell.core.data.structural import PdbAtom
from pybioshell.std import vector_core_data_basic_Vec3
from pybioshell.core.calc.structural.transformations import *

native_atoms = [" NA ", " NB ", " NC ", " ND "]
model_atoms = [" N1 ", " N2 ", " N4 ", " N3 "]

ligand_name_in_reference = sys.argv[1]
reference_file_name = sys.argv[2]
ligand_name_in_model = sys.argv[3]
model_file_names = sys.argv[4:]


def find_atom(strctr, residue_name, atom_name):
    for ci in range(strctr.count_chains()):
        c = strctr[ci]
        for ri in range(c.count_residues()):
            r = c[ri]
            if r.residue_type().code3 != residue_name: continue
            for ai in range(r.count_atoms()):
                if r[ai].atom_name() == atom_name:
                    return r[ai]
    return None


def get_atoms(strctr, residue_name):
  atoms = []
  for ci in range(strctr.count_chains()):
    c = strctr[ci]
    for ri in range(c.count_residues()):
        r = c[ri]
        if r.residue_type().code3 != residue_name: continue
        for ai in range(r.count_atoms()):
            # print(r[ai].atom_name())
            atoms.append(r[ai])
  return atoms

# ---------- Load the reference structure
reference = Pdb(reference_file_name,"is_hetero_atom is_not_water").create_structure(0)
ref_heme = vector_core_data_basic_Vec3()
for atom_name in native_atoms:
    ref_heme.append(find_atom(reference, "HEM", atom_name))
ref_ligand_atoms = get_atoms(reference, ligand_name_in_reference)
ref_ligand_atom_names = [ a.atom_name() for a in ref_ligand_atoms]
rms = CrmsdOnVec3()

# ---------- Load models one by one
for pdb_fname in model_file_names:
    model = Pdb(pdb_fname, "is_hetero_atom is_not_water").create_structure(0)
    model_heme = vector_core_data_basic_Vec3()
    for atom_name in model_atoms:
        model_heme.append(find_atom(model, "HEM", atom_name))
    crmsd_val_1 = rms.crmsd(model_heme, ref_heme, 4, True)
    print(crmsd_val_1, len(ref_heme))

    for ci in range(model.count_chains()):
        c = model[ci]
        for ri in range(c.count_residues()):
            r = c[ri]
            for ai in range(r.count_atoms()):
                rms.apply(r[ai])
                # print(r[ai].to_pdb_line())
    ref_ligand = vector_core_data_basic_Vec3()
    model_ligand = vector_core_data_basic_Vec3()
    n = 0
    for atom_name in ref_ligand_atom_names:
        m = find_atom(model, ligand_name_in_model, atom_name)
        r = find_atom(reference, ligand_name_in_reference, atom_name)
        if m and r:
            ref_ligand.append(r)
            model_ligand.append(m)
            n += 1
    crmsd_val_2 = rms.calculate_crmsd_value(ref_ligand, model_ligand, n)
    print(crmsd_val_2)


