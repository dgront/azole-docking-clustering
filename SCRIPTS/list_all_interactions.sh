#/bin/bash

# VT_1129 Eburicol
for ligand in Clotrimazole  Fluconazole Itraconazole Ketoconazole Lanosterol Obtusifoliol  Voriconazole
do
  code=`echo $ligand | cut -c 1-3`
  for fname in ../RESULTS/final_models/$ligand/*.pdb
  do
    echo $fname
    j=`dirname $fname`
    k=`basename $fname .pdb`
    python3 list_interactions.py $code $fname  2>l >$j/$k.interactions
  done
done

