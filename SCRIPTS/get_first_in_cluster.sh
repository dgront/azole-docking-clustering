#!/bin/bash

#-------------------------------------------------
#
# Run this script in RESULTS/clustering-outputs
#-------------------------------------------------

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/users/jkrys/src.git/Python-3.7.2/
DIR="/home/users/dgront/runs/Akapo-results/azole-docking-clustering/"

#for i in 00040; do
for i in `cat $DIR/INPUTS/proteins`; do
#  for j in Clotrimazole Eburicol; do 
  for j in `cat $DIR/INPUTS/ligands`; do
    ligand=`echo $j | cut -c 1-3`
    sort -nr $i/$j/clusters-5.00.txt | head -5 > s
    /home/users/jkrys/src.git/Python-3.7.2/python ../../SCRIPTS/create_first_in_a_cluster.py s $ligand $i/$j $i"-"$j"-5.0-first-"
#    sort -nr $i/$j/clusters-7.00.txt | head -5 > s
#    /home/users/jkrys/src.git/Python-3.7.2/python ../../SCRIPTS/create_first_in_a_cluster.py s $ligand $i/$j  $i"-"$j"-7.0-first-"
    rm s
  done
done
