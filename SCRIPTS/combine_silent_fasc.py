import sys
sys.path.append("visualife/src")

from data import ScoreFile, combine_score_files


if __name__ == "__main__" :

    sc = combine_score_files(*sys.argv[1:], 
            skip_from_facs=["pdb_name", "decoy", "nstruct", "angle_constraint", "atom_pair_constraint", "chainbreak",
                            "dslf_ca_dih", "dslf_cs_ang", "dslf_ss_dih", "dslf_ss_dst", "ref", "dihedral_constraint",
                            "model", "rmsd"],
            rename_columns=[("filename", "tag")])
    sc.write_score_file("out.fsc")