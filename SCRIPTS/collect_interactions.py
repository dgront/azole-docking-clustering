import sys, os, glob
from os.path import expanduser
sys.path.append(os.path.join(expanduser("~"), "src.git/visualife"))
from visualife.data import ScoreFile

# ---------- Fullsets:
#drugs = ["Clotrimazole", "Eburicol", "Fluconazole", "Itraconazole", "Ketoconazole", "Lanosterol", "Obtusifoliol", "VT_1129", "Voriconazole"]
#proteins = ["00040", "1742", "271374", "345805", "367271", "38441", "523016", "627561", "629036", "809380", "EAL23379", "KIR77383"]

# ---------- Ready so far:
drugs = ["Fluconazole"]
proteins = ["00040", "1742", "271374", "345805", "367271", "38441", "523016", "627561", "629036"]

# ---------- bioshell.pl settings
DIR = "/home/dgront/work.git/azole-docking-clustering/OUTPUTS/"
OUT = "/home/dgront/work.git/azole-docking-clustering/RESULTS/fe-distances-all/"

# ---------- mac os settings
#DIR = "/Users/dgront/work.git/azole-docking-clustering/OUTPUTS/"

for protein in proteins:
    for drug in drugs:
        path = os.path.join(DIR, protein, drug, "outputs_10000")
        print(path)
        sc = ScoreFile(columns=['contacts', 'hbonds', 'tag'])
        for json_file in glob.glob(path+"/*.json"):
            txt = open(json_file).read()
            json_file = os.path.basename(json_file).replace(".json", "").replace("contacts_", "")
            sc.add_row([txt.count(':'), 0, json_file])

        for json_file in glob.glob(path+"/intrct*"):
            n_hbonds = 0
            for line in open(json_file):
                if line.find("HYDROGEN") > -1: n_hbonds += 1
            if n_hbonds > 0:
                tag = os.path.basename(json_file).replace(".txt", "").replace("intrctns_", "")
                sc.replace_value('hbonds', tag, n_hbonds)

        path = os.path.join(OUT, protein, drug, "contacts_hbonds.sc")
        sc.write(path)
