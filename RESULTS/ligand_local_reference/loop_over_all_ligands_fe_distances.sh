INP="../../INPUTS"
for i in `cat $INP/proteins`; 
do 
  mkdir -p $i
  cd $i; 
  for j in `cat ../$INP/ligands`; 
  do 
    mkdir -p $j
    cd $j; 
#    cp /mnt/storage2/jmacnar/Akapo_docking/$i/$j/outputs_10000/$i"_HEM_"$j.pdb_1.pdb ./
#	curl -O  https://bitbucket.org/dgront/azole-docking-clustering/raw/HEAD/RESULTS/ligand_local_reference/$i/$j/local_reference
	curl -O  https://bitbucket.org/dgront/azole-docking-clustering/raw/HEAD/RESULTS/ligand_local_reference/$i/$j/outputs_10000/$i"_HEM_"$j.pdb_1.pdb
    cd ../;
  done; 
  cd ../; 
done

