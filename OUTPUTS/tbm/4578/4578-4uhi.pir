>P1;4578
sequence:4578:1    :A:496  :A::::
-DPYKFLFDCREKYGDVFTFILLGRRMTVALGPKGNNLSLGGKITHVSAEEAYTHLTTPVFGKGVVYDCPNDMLMQQKKFSGSPVCCFCGSASGSPTPRSTPARSFRRNPTNAQIKHGLTTEALSSYAELMRGETRQYFRDHVVTGKPFEVLEMMQQLIILTASRTLQGKEVRENLDIRFAKLLEDLDKGFTPVNF-LFPNLPLPSYKRRDKAQKEMSDFYMSIIEKRRSGEHDHENDMIAALQGSVYKNGVPLSDRDISHIM--------------------------ELQQMLLDEQVEKFGNPDGTFRDMTLEDTRGLPLMTASIRETLRMHAPIHSIYRKVTQDIAVPPSLAAPSKDGSYVIPKGHFIVAAPGVSQMDPQIWKDAQTWRPTRWLEEDGVANAANEQYTSGERVDYGFGAVSKGTESPYQPFGAGRHRCVGEQFAYLQLTVLVAEIIRNFKLKPVAAEFPKTNYQTMIVLPLDGRIVLEPR/..*
>P1;4UHI
structure:4UHI: :B: :B::HOMO SAPIENS:2.04:
-SPIEFLENAYEKYGPVFSFTMVGKTFTYLLGSDAAALLFNSKNEDLNAEDVYSRLTTPVFGKGVAYDVPNPVFLEQKKM----------------------------------LKSGLNIAHFKQHVSIIEKETKEYFESWGE-SGEKNVFEALSELIILTASHCLHGKEIRSQLNEKVAQLYADLDGGFSHAAWLLPGWLPLPSFRRRDRAHREIKDIFYKAIQKRRQSQE-KIDDILQTLLDATYKDGRPLTDDEVAGMLIGLLLAGQHTSSTTSAWMGFFLARDKTLQKKCYLEQKTVCGENLP---PLTYDQLKDLNLLDRCIKETLRLRPPIMIMMRMARTPQTV----------AGYTIPPGHQVCVSPTVNQRLKDSWVERLDFNPDRYLQDNPASG----------------------EKFAYVPFGAGRHRCIGENFAYVQIKTIWSTMLRLYEFDLIDGYFPTVNYTTMIHTPENPVIRYKRR/..*

