# Loop refinement of an existing model
from modeller import *
from modeller.automodel import *

log.verbose()
env = environ()

log.verbose()    # request verbose output
env = environ()  # create a new MODELLER environment to build this model in

# directories for input atom files
#env.io.atom_files_directory = ['.', '../atom_files']
#
#a = automodel(env,
#              alnfile  = '4lxj-1742.pir',     # alignment filename
#              knowns   = '4LXJ',              # codes of the templates
#              sequence = '1742')              # code of the target
#a.starting_model= 1                 # index of the first model
#a.ending_model  = 1                 # index of the last model
#                                    # (determines how many models to calculate)
#a.make()                            # do the actual comparative modeling







# directories for input atom files
env.io.atom_files_directory = './:../atom_files'
env.io.hetatm = True

#env.io.atom_files_directory = ['.', '../atom_files']
# Create a new class based on 'loopmodel' so that we can redefine
# select_loop_atoms (necessary)
class MyLoop(loopmodel):
    # This routine picks the residues to be refined by loop modeling
    print("def")
    def select_loop_atoms(self):
        # residue insertion
        print("return") 
        return selection(self.residue_range('128:A', '166:A'))

m = MyLoop(env,
           inimodel='1742.B99990001.pdb', # initial model of the target
           sequence='1742')          # code of the target

m.loop.starting_model= 1           # index of the first loop model 
m.loop.ending_model  = 10          # index of the last loop model
m.loop.md_level = refine.very_fast # loop refinement method; this yields
                                   # models quickly but of low quality;
                                   # use refine.slow for better models

m.make()




